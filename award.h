#include "common.h"
#include <vector>
class readdata {
		int total;
		ifstream fin;
	public:
		void read_name(vector<string>& StuName) {
			StuName.resize(0);
			fin.open("./configs/StuName.txt");
			string tmp;
			while (getline(fin, tmp)) {
				StuName.push_back(tmp);
			}
			fin.close();
			fin.clear();
		}
		int read_chance(vector<int>& Chance) {
            Chance.resize(0);
			fin.open("./configs/Chance.txt");
			string tmp;
			while (getline(fin, tmp)) {
				int tmpint = atoi(tmp.c_str());
				total += tmpint;
				Chance.push_back(tmpint);
			}
			fin.close();
			fin.clear();
			return total;
		}
		void read_awards(vector<string>& Awards) {
			Awards.resize(0);
			fin.open("./configs/Awards.txt");
			string tmp;
			while (getline(fin, tmp)) {
				Awards.push_back(tmp);
			}
			fin.close();
			fin.clear();
		}

};
class award_module {
	public:
		int case_award(int randnum, vector<int> chance) {
			int i = 0, endnum = 0;
			while (!(randnum <= endnum)) {
				endnum += chance[i++];
			}
			return i - 1;
		}
};
