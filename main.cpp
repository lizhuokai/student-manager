#include "award.h"
vector<string> StuName,Awards;
vector<int> Chance;
int total_Chance=0;
/* run this program using the console pauser or add your own getch, system("pause") or input loop */
void awards(){
	system("cls");
	readdata readata;
	if(total_Chance==0){
		//TODO
		readata.read_awards(Awards);
		total_Chance=readata.read_chance(Chance);
		readata.read_name(StuName);
	}//预处理阶段，读入数据
	cout<<"20班奖励系统\n请输入需要奖励的人数：\n";
	int n=0,StuNum=StuName.size();//需要奖励的学生人数 以及学生总人数
	cin>>n;
	cout<<"当前检测到："<<StuNum<<"名学生\n请输入需要奖励的人的学号\n（参考班级管理日志）：";
	vector<int> need_award;
	for(int i=0;i<n;i++) {
		int tmp=0;
		cin>>tmp;
		while(tmp>StuNum){
			cout<<"请检查学号是否正确\n请重新输入：";
			cin>>tmp;
		}
		need_award.push_back(tmp);
	}
	award_module award_work;
	for(int i=0;i<n;i++){
		cout<<i+1<<"."<<StuName[need_award[i]-1]<<"的奖励为："<<Awards[award_work.case_award(random(1,total_Chance),Chance)]<<endl;
	}
	sleep(1);
	system("pause");
}
int main() {
	system("cls");
	system("color f0");
	cout<<"指令提示：\n1.奖励系统（输入奖励或输入数字1）\n请输入：";
	string n;
	cin>>n;
	if(n=="v")
	{
		cout<<"version --1.1.1\ncreator:Bowei Li,Zhuokai Li"<<endl;
		system("pause");
	}
	if(n=="奖励"||n=="1"){
		awards();
	}
	return main();
}
